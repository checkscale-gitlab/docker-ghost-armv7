## Ghost armv7

Ghost + NGINX reverse proxy using supervisord and Docker

### Installation

1. Install [Docker](https://www.docker.com/).

2. Clone this repo, and build the image

```

docker build -t armv7-ghost .

```

### Usage

    docker run -d -p 80:80 armv7-ghost

#### Customizing Ghost

    docker run -d \
    -p 80:80 \
    -v <override-contentdir>:/www/ghost/content \
    -v <override-config_js>:/www/ghost/config.js \
    armv7-ghost

where `<override-dir>` is an absolute path of a directory that could contain:

  - `config.js`: custom config generated from [here](http://docs.ghost.org/pl/usage/configuration/) (you must replace `127.0.0.1` with `0.0.0.0`)
  - `content/data/`: persistent/shared data
  - `content/images/`: persistent/shared images
  - `content/themes/`: more themes

After few seconds, open `http://<host>` for blog or `http://<host>/ghost` for admin page.